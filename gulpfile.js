const gulp          = require('gulp');
const sass          = require('gulp-sass');
const browserSync   = require('browser-sync');
const autoprefixer  = require('gulp-autoprefixer');
const notify        = require('gulp-notify');
const concat        = require('gulp-concat');
const pug           = require('gulp-pug');
const imagemin      = require('gulp-imagemin');
const uglify        = require('gulp-uglify');
const babel         = require('gulp-babel');
const zip           = require('gulp-zip');
const cleanCSS      = require('gulp-clean-css');



// Sass
gulp.task('styles', function() {
	return gulp.src('app/sass/**/*.sass')
		.pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
		// .pipe(rename({ suffix: '.min', prefix : '' }))
		.pipe(autoprefixer(['last 2 versions']))
		// .pipe(cleanCSS( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.stream());
});


// JS
gulp.task('scripts', function() {
	return gulp.src([
		'app/js/libs/jquery.min.js',
		'app/js/libs/slick.min.js',
		'app/js/common.js' // Всегда в конце
		])
		.pipe(concat('scripts.min.js'))
		// .pipe(babel({
		// 	presets: ['@babel/env']
		// }))
		// .pipe(uglify()) // Минификатор
		.pipe(gulp.dest('app/js'))
		.pipe(browserSync.stream());
});


// Pug
gulp.task('pug', function() {
	return gulp.src('app/pug/*.pug')
		.pipe(pug({
			pretty: true // Минификатор html кода
		}))
		.pipe(gulp.dest('app/'))
		.pipe(browserSync.stream());
});


// Img
gulp.task('img', function() {
	return gulp.src('app/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/img'))
});


// Browser-Sync
gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false,
		// plugins: ['bs-console-qrcode'],
		host: '192.168.0.106' // Сменить адрес, если сервер не работает на мобильном 
		// online: true, // Если нет интернета
		// tunnel: true, tunnel: "developerrokkido" // Демонстрация заказчику: http://developerrokkido.localtunnel.me
	});
});

// archive
gulp.task('zip', function(){
	return gulp.src('app/**/*')
		.pipe(zip('archive.zip'))
		.pipe(gulp.dest('./'))
});

// Watch
gulp.task('watch', function(){
	gulp.watch('app/sass/**/*.sass', gulp.parallel('styles'));
	gulp.watch(['app/js/**/*.js', '!app/js/scripts.min.js'], gulp.parallel('scripts'));
	gulp.watch('app/pug/**/*.pug', gulp.parallel('pug'));
	// gulp.watch('app/*.html').on('change', browserSync.reload);
});


gulp.task('default', gulp.parallel('styles', 'scripts', 'pug', 'browser-sync', 'watch'));